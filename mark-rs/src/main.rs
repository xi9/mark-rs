mod cli;
mod display;
// magic from gl_generator
mod gl {
    #![allow(clippy::all)]
    include!(concat!(env!("OUT_DIR"), "/gl_bindings.rs"));
}

fn main() {
    display::test_fn();
}
